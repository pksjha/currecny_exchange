 
$(document).ready(function() {
	
	// jQuery "#ID" SELECTOR.
	$('#base_ammount').keypress(function (event) {
		return isNumber(event, this)
	});
});
// end of document ready function

// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (
			(charCode != 8) && (charCode != 9) && (charCode != 13) &&
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }    
