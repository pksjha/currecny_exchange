<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    'log_level' => env('APP_LOG_LEVEL', 'debug'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Illuminate\Html\HtmlServiceProvider::class,

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Form'  => Illuminate\Html\FormFacade::class,
        'HTML'  => Illuminate\Html\HtmlFacade::class,

    ],

    //all currencies data
    'currencies' => [
        '' => '--Select--',
        'ALL' => 'ALL - Albania Lek',
        'AFN' => 'AFN - Afghanistan Afghani',
        'ARS' => 'ARS - Argentina Peso',
        'AWG' => 'AWG - Aruba Guilder',
        'AUD' => 'AUD - Australia Dollar',
        'AZN' => 'AZN - Azerbaijan New Manat',
        'BSD' => 'BSD - Bahamas Dollar',
        'BBD' => 'BBD - Barbados Dollar',
        'BDT' => 'BDT - Bangladeshi taka',
        'BYR' => 'BYR - Belarus Ruble',
        'BZD' => 'BZD - Belize Dollar',
        'BMD' => 'BMD - Bermuda Dollar',
        'BOB' => 'BOB - Bolivia Boliviano',
        'BAM' => 'BAM - Bosnia and Herzegovina Convertible Marka',
        'BWP' => 'BWP - Botswana Pula',
        'BGN' => 'BGN - Bulgaria Lev',
        'BRL' => 'BRL - Brazil Real',
        'BND' => 'BND - Brunei Darussalam Dollar',
        'KHR' => 'KHR - Cambodia Riel',
        'CAD' => 'CAD - Canada Dollar',
        'KYD' => 'KYD - Cayman Islands Dollar',
        'CLP' => 'CLP - Chile Peso',
        'CNY' => 'CNY - China Yuan Renminbi',
        'COP' => 'COP - Colombia Peso',
        'CRC' => 'CRC - Costa Rica Colon',
        'HRK' => 'HRK - Croatia Kuna',
        'CUP' => 'CUP - Cuba Peso',
        'CZK' => 'CZK - Czech Republic Koruna',
        'DKK' => 'DKK - Denmark Krone',
        'DOP' => 'DOP - Dominican Republic Peso',
        'XCD' => 'XCD - East Caribbean Dollar',
        'EGP' => 'EGP - Egypt Pound',
        'SVC' => 'SVC - El Salvador Colon',
        'EEK' => 'EEK - Estonia Kroon',
        'EUR' => 'EUR - Euro Member Countries',
        'FKP' => 'FKP - Falkland Islands (Malvinas) Pound',
        'FJD' => 'FJD - Fiji Dollar',
        'GHC' => 'GHC - Ghana Cedis',
        'GIP' => 'GIP - Gibraltar Pound',
        'GTQ' => 'GTQ - Guatemala Quetzal',
        'GGP' => 'GGP - Guernsey Pound',
        'GYD' => 'GYD - Guyana Dollar',
        'HNL' => 'HNL - Honduras Lempira',
        'HKD' => 'HKD - Hong Kong Dollar',
        'HUF' => 'HUF - Hungary Forint',
        'ISK' => 'ISK - Iceland Krona',
        'INR' => 'INR - India Rupee',
        'IDR' => 'IDR - Indonesia Rupiah',
        'IRR' => 'IRR - Iran Rial',
        'IMP' => 'IMP - Isle of Man Pound',
        'ILS' => 'ILS - Israel Shekel',
        'JMD' => 'JMD - Jamaica Dollar',
        'JPY' => 'JPY - Japan Yen',
        'JEP' => 'JEP - Jersey Pound',
        'KZT' => 'KZT - Kazakhstan Tenge',
        'KPW' => 'KPW - Korea (North) Won',
        'KRW' => 'KRW - Korea (South) Won',
        'KGS' => 'KGS - Kyrgyzstan Som',
        'LAK' => 'LAK - Laos Kip',
        'LVL' => 'LVL - Latvia Lat',
        'LBP' => 'LBP - Lebanon Pound',
        'LRD' => 'LRD - Liberia Dollar',
        'LTL' => 'LTL - Lithuania Litas',
        'MKD' => 'MKD - Macedonia Denar',
        'MYR' => 'MYR - Malaysia Ringgit',
        'MUR' => 'MUR - Mauritius Rupee',
        'MXN' => 'MXN - Mexico Peso',
        'MNT' => 'MNT - Mongolia Tughrik',
        'MZN' => 'MZN - Mozambique Metical',
        'NAD' => 'NAD - Namibia Dollar',
        'NPR' => 'NPR - Nepal Rupee',
        'ANG' => 'ANG - Netherlands Antilles Guilder',
        'NZD' => 'NZD - New Zealand Dollar',
        'NIO' => 'NIO - Nicaragua Cordoba',
        'NGN' => 'NGN - Nigeria Naira',
        'NOK' => 'NOK - Norway Krone',
        'OMR' => 'OMR - Oman Rial',
        'PKR' => 'PKR - Pakistan Rupee',
        'PAB' => 'PAB - Panama Balboa',
        'PYG' => 'PYG - Paraguay Guarani',
        'PEN' => 'PEN - Peru Nuevo Sol',
        'PHP' => 'PHP - Philippines Peso',
        'PLN' => 'PLN - Poland Zloty',
        'QAR' => 'QAR - Qatar Riyal',
        'RON' => 'RON - Romania New Leu',
        'RUB' => 'RUB - Russia Ruble',
        'SHP' => 'SHP - Saint Helena Pound',
        'SAR' => 'SAR - Saudi Arabia Riyal',
        'RSD' => 'RSD - Serbia Dinar',
        'SCR' => 'SCR - Seychelles Rupee',
        'SGD' => 'SGD - Singapore Dollar',
        'SBD' => 'SBD - Solomon Islands Dollar',
        'SOS' => 'SOS - Somalia Shilling',
        'ZAR' => 'ZAR - South Africa Rand',
        'LKR' => 'LKR - Sri Lanka Rupee',
        'SEK' => 'SEK - Sweden Krona',
        'CHF' => 'CHF - Switzerland Franc',
        'SRD' => 'SRD - Suriname Dollar',
        'SYP' => 'SYP - Syria Pound',
        'TWD' => 'TWD - Taiwan New Dollar',
        'THB' => 'THB - Thailand Baht',
        'TTD' => 'TTD - Trinidad and Tobago Dollar',
        'TRY' => 'TRY - Turkey Lira',
        'TRL' => 'TRL - Turkey Lira',
        'TVD' => 'TVD - Tuvalu Dollar',
        'UAH' => 'UAH - Ukraine Hryvna',
        'GBP' => 'GBP - United Kingdom Pound',
        'UGX' => 'UGX - Uganda Shilling',
        'USD' => 'USD - United States Dollar',
        'UYU' => 'UYU - Uruguay Peso',
        'UZS' => 'UZS - Uzbekistan Som',
        'VEF' => 'VEF - Venezuela Bolivar',
        'VND' => 'VND - Viet Nam Dong',
        'YER' => 'YER - Yemen Rial',
        'ZWD' => 'ZWD - Zimbabwe Dollar'
    ],

];
