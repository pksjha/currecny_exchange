<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use HTML,Form,Validator,Mail,Response,Session,Auth,DB,Redirect,Image, File,View,Config;

use App\Classes\ExchangeRate;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $data['currencies'] = Config::get('app.currencies');

        #return to the view file
        return view('site.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        $data = [];
        #assign rules for validation
        $rules = [
            'base_currency' => 'required',
            'base_ammount' => 'required',
            'target_currency' => 'required',
        ];
        #assign message for validation field
        $messages = [
            'base_currency.required' => 'Please select a Base currency',
            'base_ammount.required' => 'Please enter some ammount',
            'target_currency.required' => 'Please select a Target currency',
        ];

        $validator = Validator::make($request->all(), $rules,$messages);
        #check validation
        if ($validator->fails()){
            return Redirect::back()->withErrors($validator);
        }
        else{
            $data['currencies'] = Config::get('app.currencies');
            $base_currency = $data['base_currency'] = $request->base_currency;
            $base_ammount = $data['base_ammount'] = $request->base_ammount;
            $target_currency = $data['target_currency'] = $request->target_currency;
            $rate = new ExchangeRate();
            $data['result'] = $rate->getExchange($base_currency,$base_ammount,$target_currency,'fixer');

            return view('site.result',$data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
