<?php
namespace App\Classes;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ExchangeRate {

    public function getExchange($base_currency,$base_ammount,$target_currency, $api = "fixer") {
        if ($api == 'fixer') {

            // API credntials and its call
            $access_key = 'f844d1dd3b258af6931bee95240ce833';
            $url = "http://data.fixer.io/api/latest?access_key=".$access_key."&base=".$base_currency."&symbols=".$target_currency."&format=1";

            $client = new Client();
            $response = $client->request('GET', $url);

            $result_decoded = json_decode($response->getBody());
            if($result_decoded->success){
                $result_decoded->converted_amount = number_format($result_decoded->rates->$target_currency * $base_ammount, 2);

                $result_decoded->complete_response = $result_decoded; // This can be used to dump the data in database for further investigation in case of any data discrepency

                //return the result now
                return $result_decoded;
            }

        }
        else if ($api == 'yahoo') { // We can write the API calls here for any other provider e.g yahoo

            // Api calls goes here
        }


       return FALSE;

    }

}
