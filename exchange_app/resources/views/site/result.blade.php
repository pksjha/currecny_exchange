@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="panel-heading">
               <div class="panel-title text-center">
                    <h3 class="title">Exchange Rates App</h3>
                    <hr />
                </div>
            </div>

            <div class="panel-body">
                <div class="form-group">
                    <div class="cols-sm-12 text-center" style="font-size: 20px;font-family: 'Oxygen', sans-serif;font-weight: 400 !important;">
                        @if($result != FALSE)
                            {{$base_ammount ." ". $base_currency ." = ". $result->converted_amount." ". $target_currency}}
                        @else
                            No data to display.
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="cols-sm-12">
                    <a href="{{ url('/') }}" class="btn btn-primary btn-lg btn-block login-button">Go Back!</a>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>


@endsection
