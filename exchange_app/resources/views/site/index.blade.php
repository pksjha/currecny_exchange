@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="panel-heading">
               <div class="panel-title text-center">
                    <h3 class="title">Exchange Rates App</h3>
                    <hr />
                </div>
            </div>

            <div class="panel-body">
                {!! Form::open(['action' => 'SiteController@generate', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'exchange-rate-form', 'name' => 'exchange-rate-form']) !!}

                    <div class="form-group <?php if ($errors->has('base_currency')) {echo 'has-error';}?>">
                        <label for="name" class="cols-sm-2 control-label">Select Base currency</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                {!! Form::select('base_currency', $currencies, 'EUR', [
                                    'class' => 'form-control',
                                    'id'      => 'base_currency',
                                ]) !!}
                            </div>
                            @if ($errors->has('base_currency'))
                                <div class="error">{{ $errors->first('base_currency') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group <?php if ($errors->has('base_ammount')) {echo 'has-error';}?>">
                        <label for="email" class="cols-sm-2 control-label">Base amount</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                {!! Form::text('base_ammount', null, [
                                    'class'          => 'form-control',
                                    'placeholder' => 'Base amount',
                                    'id'               => 'base_ammount'
                                ]) !!}
                            </div>
                            @if ($errors->has('base_ammount'))
                                <div class="error">{{ $errors->first('base_ammount') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group <?php if ($errors->has('target_currency')) {echo 'has-error';}?>">
                        <label for="name" class="cols-sm-2 control-label">Select Target currency</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></span>
                                {!! Form::select('target_currency', $currencies, null, [
                                    'class' => 'form-control',
                                    'id'      => 'target_currency',
                                ]) !!}
                            </div>
                            @if ($errors->has('target_currency'))
                                <div class="error">{{ $errors->first('target_currency') }}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Submit' ,['class' => 'btn btn-primary btn-lg btn-block login-button',]) !!}
                    </div>
                {!! Form::close() !!}
            </div>

        </div>
        <div class="col-md-4"></div>
    </div>
</div>


@endsection
