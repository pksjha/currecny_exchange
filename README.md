
## The application is developed using Laravel 5.6

Here are the steps to run the application to see the functionalities

- Download the zip file and extract it to your web server root folder.
- If you are running the project under any sub folder, you will need to set the base path in public/.htaccess file. The base path is set in line 9 of the .htaccess shipped with the project. You can change that accordingly.
- You can run the application by simply going to browser i.e. http://localhost/folder_name/public 
- If you are configuring the application on some server using domain or sub-domain, you will need to place the files under appropriate folder and set the document root path accordingly. If you place the files under some sub-folder, you will need to set the base path in .htaccess file otherwise you can set it as '/' in base path.
- Since all the vendors are included in vendor folder, it should work fine. If it does not work properly or throws any error related to any vendor, you can re-install or update the vendors by running composer commands i.e. composer update or composer install . 
